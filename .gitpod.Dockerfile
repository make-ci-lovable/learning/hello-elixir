# Elixir for GitPod and GitLab 2020-11-01 by @k33g | on gitlab.com 
FROM elixir:1.10.4-alpine

LABEL maintainer="@k33g_org"
LABEL authors="@k33g_org"
LABEL version="1.0"

# 🚧 wip
RUN apk --update add --no-cache curl

CMD ["/bin/sh"]
